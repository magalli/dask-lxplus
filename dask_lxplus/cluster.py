import logging

from collections import ChainMap
import dask
from dask_jobqueue import HTCondorCluster
from dask_jobqueue.htcondor import HTCondorJob


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

def merge(*args):
    # This will merge dicts, but earlier definitions win
    return dict(ChainMap(*filter(None, args)))

class CernJob(HTCondorJob):
    config_name = "cern"

    def __init__(self,
                 scheduler=None,
                 name=None,
                 disk=None,
                 **base_class_kwargs
                 ):

        if disk is None:
            num_cores = base_class_kwargs.get("cores", 1)
            disk = f'{int(num_cores) * 20} GB'

        super().__init__(scheduler=scheduler, name=name, disk=disk, **base_class_kwargs)

        self.job_header_dict.update(
            {
                "when_to_transfer_output": "ON_EXIT_OR_EVICT",
                "transfer_output_files": "",
            }
        )
        if hasattr(self, "log_directory"):
            if self.log_directory:
                self.job_header_dict.pop("Stream_Output", None)
                self.job_header_dict.pop("Stream_Error", None)
                self.job_header_dict.update({"My.SpoolOnEvict": False})


class CernCluster(HTCondorCluster):
    __doc__ = (
        HTCondorCluster.__doc__
    + """
    A customized :class:`dask_jobqueue.HTCondorCluster` subclass for spawning Dask workers in the CERN HTCondor pool

    It provides the customizations and submit options required for the CERN pool.
    
    Additional CERN parameters:
    worker_image: The container to run the Dask workers inside. Defaults to FIXME
    batch_name: The HTCondor JobBatchName assigned to the worker jobss. The default ends up as ``"dask-worker"``
    """
    )
    config_name = "cern"
    job_cls = CernJob

    # note this is where we'd specify a CERN override of the job definition
    # job_cls = CernJob

    def __init__(self,
                 *,
                 worker_image = None,
                 image_type = None,
                 gpus = None,
                 batch_name = None,
                 **base_class_kwargs,
                 ):
        """
        :param: worker_image: The container image to run the Dask workers inside.
        Defaults to the singularity image #FIXME add image name here
        :param: image_type: The image type to run the Dask workers inside. Either
        ``docker`` or ``singularity``, defaults to singularity.
        :param: disk: The amount of disk to request. Defaults to 20 GiB / core
        :param: gpus: The number of GPUs to request.
        Defaults to ``0``.
        :param batch_name: The HTCondor JobBatchName ro assign to the worker jobs.
        The default ends up as ``"dask-worker"``
        :param kwargs: Additional keyword arguments like ``cores`` or ``memory`` to pass
        to `dask_jobqueue.HTCondorCluster`.
        """

        base_class_kwargs = self._modify_kwargs(
            base_class_kwargs,
            worker_image=worker_image,
            image_type=image_type,
            gpus=gpus,
            batch_name=batch_name,
        )

        super().__init__(**base_class_kwargs)

    @classmethod
    def _modify_kwargs(cls,
                       kwargs,
                       *,
                       worker_image = None,
                       image_type = None,
                       gpus = None,
                       batch_name = None,
                       ):
        """
        This method implements the special modifications to adapt dask-jobqueue to run on the CERN cluster.

        See the class __init__ for the details of the arguments.
        """
        modified = kwargs.copy()

        image_type = image_type or dask.config.get(f"jobqueue.{cls.config_name}.image-type")
        worker_image = worker_image or dask.config.get(f"jobqueue.{cls.config_name}.worker-image")

        modified["job_extra"] = merge(
            {"universe": "docker" if image_type == "docker" else "vanilla"},
            {"docker_image": f'"{worker_image}"'} if image_type == "docker" else None,
            {"MY.SingularityImage": f'"{worker_image}"'} if image_type == "singularity" else None,
            {"request_gpus": str(gpus)} if gpus is not None else None,
            {"MY.IsDaskWorker": "true"},
            # extra user input
            kwargs.get("job_extra", dask.config.get(f"jobqueue.{cls.config_name}.job-extra")),
            {"JobBatchName": f'"{batch_name or dask.config.get(f"jobqueue.{cls.config_name}.batch-name")}"'},
        )

        modified["extra"] = [
            *kwargs.get("extra", dask.config.get(f"jobqueue.{cls.config_name}.extra")),
            "--worker-port",
            "10000:10100",
        ]
        return modified


